<?php

class SimplicateApi {

    /** @var string */
    public $authenticationKey;

    /** @var string */
    public $authenticationSecret;

    /** @var string */
    public $apiUrl;

    /**
     * @param string $domain
     * @param string $key
     * @param string $secret
     */
    public function __construct($domain, $key, $secret) {
        $this->authenticationKey = $key;
        $this->authenticationSecret = $secret;
        $this->apiUrl = 'https://' . $domain . '/api/v2';
    }

    /**
     * @param string $method
     * @param string $url
     * @param null   $payload
     *
     * @return array|mixed
     */
    public function makeApiCall($method, $url, $payload = null) {
        // Generate the list of headers to always send.
        $headers = [
            'User-Agent: simplicate-koppeling',
            'Authentication-Key: ' . $this->authenticationKey,
            'Authentication-Secret: ' . $this->authenticationSecret,
            'Accept: application/json',
        ];

        $endpoint = $this->apiUrl . $url;

        $curl = curl_init($endpoint);

        switch(strtoupper($method)) {
            case 'GET':
                break;
            case 'POST':
                $headers[] = 'Content-Type: application/json';
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
                break;
            case 'PUT':
                $headers[] = 'Content-Type: application/json';
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
                break;
            case 'DELETE':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
            default:
                exit;
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if(self::isFailure($httpCode)) {
            return [
                'errorNumber' => $httpCode,
                'error'       => 'Request returned HTTP error ' . $httpCode,
                'requestUrl'  => $url,
            ];
        }

        $curl_errno = curl_errno($curl);
        $curl_err = curl_error($curl);

        if($curl_errno) {
            $msg = $curl_errno . ": " . $curl_err;
            curl_close($curl);

            return [
                'errorNumber' => $curl_errno,
                'error'       => $msg,
            ];
        } else {
            error_log('Response: ' . $response);
            curl_close($curl);

            return json_decode($response, true);
        }
    }

    /**
     * @param int $httpStatus
     *
     * @return bool
     */
    public static function isFailure($httpStatus) {
        return ($httpStatus >= 400);
    }
}

$simplicateApi = new SimplicateApi('yourdomain', 'yourapikey', 'yourapisecret');

// Prepare the payload to create an organization
$org_payload = [
    'name'             => $variable_with_organization_name,
    'phone'            => $variable_with_organization_phone,
    'email'            => $variable_with_organization_email,
    'note'             => $variable_with_note,
    'relation_type'    => [
        'id' => '' // Provide the relationtype id, ex. 'relationtype:796ce0d318a2f5db515efc18bba82b90'
    ],
    'visiting_address' => [
        'country_code' => 'NL',
    ],
    'postal_address'   => [
        'country_code' => 'NL',
    ],
];

// Add the organization to the CRM
$organization = $simplicateApi->makeApiCall('POST', '/crm/organization', json_encode($org_payload));

// Fetch the newly created organization id
$organizationId = $organization['data']['id'];

// Prepare the payload to create a contact
$pers_payload = [
    'gender'                            => 'Unknown', // or Male / Female
    'first_name'                        => $variable_with_first_name,
    'family_name'                       => $variable_with_family_name,
    'email'                             => $variable_with_email,
    'linked_as_contact_to_organization' => [
        // Please note: this is an array of objects
        [
            'organization_id' => $organizationId, // add the person as a contact to the newly created organization
            'work_email'      => $variable_with_email,
            'work_phone'      => $variable_with_phone,
        ],
    ],
    'visiting_address'                  => [
        'country_code' => 'NL',
    ],
    'postal_address'                    => [
        'country_code' => 'NL',
    ],
];

// Add the person to CRM
$person = $simplicateApi->makeApiCall('POST', '/crm/person', json_encode($pers_payload));

// Fetch the newly created contact id
$contact = $simplicateApi->makeApiCall('GET', '/crm/person?q[id]=' . $person['data']['id']);
$contactId = $contact['data'][0]['linked_as_contact_to_organization'][0]['id'];

// Prepare the sales payload
$salesPayload = [
    'organization_id'       => $organizationId,
    'contact_id'            => $contactId,
    'status'                => 'Open',
    'subject'               => $variable_with_sales_subject,
    'start_date'            => date("Y-m-d"), // Set start date to today
    'expected_closing_date' => date("Y-m-d", strtotime(date("Y-m-d") . '+14 days')),
    'expected_revenue'      => $variable_with_expected_revenue, // Set the expected revenue
    'note'                  => $variable_with_note,
    'progress'              => [
        'id' => 'salesprogress:200a53bf6d2bbbfe' // Fill a valid salesprogress id to set proper sales progress
    ],
];

// Create the sales
$sales = $simplicateApi->makeApiCall('POST', '/sales/sales', json_encode($salesPayload));
